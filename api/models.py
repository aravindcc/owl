from django.db import models
from enum import Enum
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class RoleEnum(Enum):
    TEACHER = "Teacher"
    STUDENT = "Student"
    CREATOR = "Creator"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class Profile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    role = models.CharField(max_length=10, choices=RoleEnum.choices())

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance, role=RoleEnum.STUDENT.name)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
