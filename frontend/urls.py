from django.urls import re_path
from django.conf import settings
from django.views.static import serve
from django.shortcuts import render


def put_up_react_app(request):
    path = request.get_full_path().replace("/", "")
    try:
        if path == "index.html":
            raise Exception('use template')
        return serve(request, path, settings.STATIC_ROOT)
    except:
        return render(request, "index.html", {})


urlpatterns = [
    re_path(r'^.*', put_up_react_app),
]
