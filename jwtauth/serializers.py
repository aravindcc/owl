from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

# These are the JSON that will be returned


# This is for normal requests
class UserSerializer(serializers.ModelSerializer):

    # role of user
    role = serializers.SerializerMethodField()

    def get_role(self, obj):
        return obj.profile.role

    class Meta:
        model = User
        fields = ('username', 'role')


# This is for signup requests
class UserSerializerWithToken(serializers.ModelSerializer):

    # token not a model field so stored with serialiser (it isn't persisted either)
    token = serializers.SerializerMethodField()
    # specify the field as write only so isn't returned in the json
    password = serializers.CharField(write_only=True)

    # get a token given a the user object
    def get_token(self, obj):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token

    # it will create model instance and handle proper hashing of password
    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    # specify model and fields of the model to be serialised.
    class Meta:
        model = User
        fields = ('token', 'username', 'password')
