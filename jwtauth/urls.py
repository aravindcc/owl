from django.urls import path
from .views import current_user, CreateUser

urlpatterns = [
    path('current/', current_user),
    path('create/', CreateUser.as_view())
]
