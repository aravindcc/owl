import uuid
import os


# Check Path exists.
# If not creates it. If file path will strip to folder.
# Creates intermediate folders as well.
def createInterimPath(path):
    try:
        dirname = os.path.dirname(path)
        print("CHECKING FOR DIR: " + dirname)
        if not os.path.exists(dirname):
            print("MAKING DIR: " + dirname)
            os.makedirs(dirname + "/")
    except Exception as e:
        print(str(e))
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        print("FAILED CREATING INTERIM PATH")


# Save a FILE object to path
def saveFile(f, path):
    createInterimPath(path)
    print("USING DIR: " + path)
    with open(path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


# save a media file !
def saveMedia(filename, fileObj):
    comps = filename.split(".")
    ext = comps[-1].lower()
    path = "/store/" + str(uuid.uuid4()) + "." + ext
    localpath = "/private/var/clar3/Desktop/Projects/dove/owl"
    saveFile(fileObj, localpath + path)
    return path
