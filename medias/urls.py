from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from . import views


should_exempt_csrf = csrf_exempt if settings.DEBUG else (lambda a: a)
urlpatterns = [
    path('upload', should_exempt_csrf(views.uploadView), name='media_get')
]
