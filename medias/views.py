from rest_framework.response import Response
from rest_framework.decorators import api_view
from .core import *

import sys
import os


@api_view(['POST'])
def uploadView(request):
    fileObj = request.FILES["file"]
    filename = fileObj.name
    new_m = saveMedia(filename, fileObj)
    return Response({
        "url": new_m
    })
