"""owl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import re_path, path, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from django.conf.urls.static import static
from paper.views import CreateSubmission

urlpatterns = [
    path('admin/', admin.site.urls),
    path('token-auth/', obtain_jwt_token),
    path('refresh-token/', refresh_jwt_token),
    path('user/', include('jwtauth.urls')),
    path('media/', include('medias.urls')),
    path('paper/', include('paper.urls')),
    path('submission/', CreateSubmission.as_view()),
] + static("/store/", document_root="/private/var/clar3/Desktop/Projects/dove/owl/store") + [re_path(r'^.*', include('frontend.urls')), ]
