from django.contrib import admin
from .models import Paper, Submission

# Register your models here.
admin.site.register(Paper)
admin.site.register(Submission)
