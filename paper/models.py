from django.db import models
import uuid
from django.db.models.signals import pre_save
from django.dispatch import receiver
import re
from django.contrib.auth.models import User


# Create your models here.
# paperID, configJSON, canavsJSON, paperName
class Paper(models.Model):
    json = models.TextField(blank=True, null=True)
    paperName = models.CharField(max_length=64, unique=True)
    urlName = models.CharField(
        max_length=64, blank=True, null=True, unique=True)

    def __str__(self):
        return self.paperName


class Submission(models.Model):
    paper = models.ForeignKey(Paper, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    json = models.TextField(blank=True, null=True)


def makeSafe(name):
    return re.sub(r'[^a-zA-Z0-9_-]+', '', name.replace(' ', '-'))


@receiver(pre_save, sender=Paper)
def compute_url_name(sender, instance, **kwargs):
    instance.urlName = makeSafe(instance.paperName)
