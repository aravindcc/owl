from .models import Paper, Submission
from rest_framework import serializers
import json
from api.models import RoleEnum
from random import shuffle


class PaperSerializer(serializers.ModelSerializer):
    def mask_json(self, obj):
        input_json = obj.json
        # strip json of values
        try:
            user_role = self.context['request'].user.profile.role
            if user_role == RoleEnum.CREATOR.name or input_json == "":
                return input_json
            final_config = json.loads(input_json)
            total_config = final_config["qs"]
            for i in range(len(total_config)):
                config = total_config[i]["config"]
                for key in config.keys():
                    element = config[key]
                    if type(element) is not dict:
                        continue
                    all_strip = ["value", "whole", "numerator", "denominator"]
                    for strip in all_strip:
                        if strip in element:
                            element[strip] = ""
                    if "order" in element:
                        org_order = element["order"].copy()
                        while org_order == element["order"]:
                            shuffle(element["order"])
                        element["OGOrder"] = element["order"].copy()
            out_json = json.dumps(final_config)
            if out_json != None:
                return out_json
        except Exception as e:
            print("FAILED BECAUSE " + str(e))
        return ""

    def to_representation(self, obj):
        data = super(PaperSerializer, self).to_representation(obj)
        data['json'] = self.mask_json(obj)
        return data

    class Meta:
        model = Paper
        fields = ['urlName', 'paperName', 'json']


class MiniPaperSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paper
        fields = ['urlName', 'paperName']


class SubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = ['paper', 'user', 'json']
