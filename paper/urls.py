from django.urls import path
from .views import PaperViewSet, CreateSubmission
from django.views.decorators.csrf import csrf_exempt
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('', PaperViewSet, basename='papers')
urlpatterns = router.urls
