from django.db import IntegrityError
from .models import Paper, Submission
from .serializers import MiniPaperSerializer, PaperSerializer, SubmissionSerializer
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from api.models import RoleEnum


class CreatorPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and (request.user.profile.role == RoleEnum.CREATOR.name or request.user.is_superuser)


class PaperViewSet(ModelViewSet):
    queryset = Paper.objects.all()
    lookup_field = 'urlName'

    def get_serializer_class(self):
        if self.action == 'list':
            return MiniPaperSerializer
        return PaperSerializer

    def create(self, request, *args, **kwargs):
        try:
            return super(ModelViewSet, self).create(request, *args, **kwargs)
        except IntegrityError:
            content = {'error': 'IntegrityError'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            return super(ModelViewSet, self).update(request, *args, **kwargs)
        except IntegrityError:
            content = {'error': 'IntegrityError'}
            return Response(content, status=status.HTTP_400_BAD_REQUEST)

    def get_permissions(self):
        original_perms = super(ModelViewSet, self).get_permissions()
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            # which is permissions.IsAdminUser
            return [CreatorPermission(), ] + original_perms
        else:
            return original_perms


class CreateSubmission(APIView):
    """
    Create a new submission.
    """

    def post(self, request, format=None):
        try:
            input_data = request.data.copy()
            urlName = input_data["urlName"]
            input_data["paper"] = Paper.objects.get(urlName=urlName).id
            input_data["user"] = request.user.id
            serializer = SubmissionSerializer(data=input_data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            print("FAILED TO CREATE SUBMISSION: " + str(e))
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
